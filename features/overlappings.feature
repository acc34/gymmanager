Feature: Session creation overlappings
  Background: Existing sessions
    Given a session of activity1 by trainer1 exists in room1 at reference_time()

  Scenario: Create a session at the same time and room
    When trainer2 creates a session of activity1 in room1 at reference_time()
    Then 1 sessions exist

  Scenario: Create a session at the same room and different time
    When trainer2 creates a session of activity1 in room1 at reference_time(days=2)
    Then 2 sessions exist

  Scenario: Create a session at the same time and different room
    When trainer2 creates a session of activity1 in room2 at reference_time()
    Then 2 sessions exist

  Scenario: Create a session at the same time, different room, same trainer
    When trainer1 creates a session of activity1 in room2 at reference_time()
    Then 1 sessions exist