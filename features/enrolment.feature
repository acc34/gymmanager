Feature: Client enrolment
  Background: Existing sessions
    Given a session of activity1 by trainer1 exists in room1 at reference_time()

  Scenario: Normal enrolment
    When client1 enrols to session 1
    Then 1 enrolments exist

  Scenario: Two enrolments at the same time
    Given a session of activity1 by trainer2 exists in room2 at reference_time()
    And an enrolment of client1 to session 2
    When client1 enrols to session 1
    Then 1 enrolments exist

  Scenario: Enrolment to running session
    Given a session of activity1 by trainer1 exists in room1 at reference_time(before=True)
    When client1 enrols to session 2
    Then 0 enrolments exist

  Scenario: Trainer tries to enrol
    When trainer2 enrols to session 1
    Then 0 enrolments exist