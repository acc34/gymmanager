import os
import django
from django.test import RequestFactory
from django.test.runner import DiscoverRunner
from django.test.testcases import LiveServerTestCase
from django.core.management import call_command

os.environ["DJANGO_SETTINGS_MODULE"] = "GymManager.settings"


def before_all(context):
    django.setup()
    context.test_runner = DiscoverRunner()
    context.test_runner.setup_test_environment()
    context.request_factory = RequestFactory()


def before_scenario(context, scenario):
    from ActivityManager.models import Activity, User, Room
    from django.contrib.auth.models import Group

    context.database_config = context.test_runner.setup_databases()
    context.test = LiveServerTestCase
    context.test.setUpClass()
    Activity.objects.create(name="activity1", max_assistants=10)
    trainers = Group.objects.create(name="trainer")
    clients = Group.objects.create(name="client")
    User.objects.create_user(username="trainer1", password="pass").groups.add(trainers)
    User.objects.create_user(username="trainer2", password="pass").groups.add(trainers)
    User.objects.create_user(username="client1", password="pass").groups.add(clients)
    Room.objects.create(name="room1", max_assistants=20)
    Room.objects.create(name="room2", max_assistants=15)


def after_scenario(context, scenario):
    context.test.tearDownClass()
    del context.test
    context.test_runner.teardown_databases(context.database_config)
    del context.database_config
    call_command('flush', verbosity=0, interactive=False)


def after_all(context):
    context.test_runner.teardown_test_environment()
