from behave import *

URL = 'http://localhost:8000/activities'


def reference_time(days=0, before=False):
    """
    Return a valid Wednesday in the middle of opening hours
    :param days: The number of days after the first valid time.
    :param before: If True, the valid time is searched back in time.
    :return: A datetime depicting a valid time.
    """
    from django.conf import settings
    from datetime import datetime, date, time, timedelta

    if abs(days) > 2:
        raise ValueError("Days has to be in [-2, 2]")

    # Time
    open_time, close_time = settings.OPEN_HOURS
    half_time = open_time + (close_time - open_time) // 2

    # Day
    # If it's Mon or Tue, this week's Wed can be used
    if date.today().weekday() < 2:
        wednesday = date.today() + timedelta(days=2-date.today().weekday())
    # If it's Wed to Sun, we use next week's Wed
    else:
        wednesday = date.today() - timedelta(days=date.today().weekday()-2) + timedelta(weeks=1)

    if before:
        wednesday = wednesday - timedelta(weeks=2)  # To make sure we cannot get a date later than today with "days"

    day = wednesday + timedelta(days=days)

    return datetime.combine(day, time(hour=half_time))


@given('a session of {activity} by {trainer} exists in {place} at {time}')
def step_impl(context, activity, trainer, place, time):
    from ActivityManager.models import Session, User, Activity, Room

    Session.objects.create(
        activity=Activity.objects.get(name=activity),
        room=Room.objects.get(name=place),
        trainer=User.objects.get(username=trainer),
        timestamp=eval(time)
    )


@given('an enrolment of {client} to session {sess_num}')
def step_impl(context, client, sess_num):
    from ActivityManager.models import Session, User, SessionUserControl
    SessionUserControl.objects.create(
        user=User.objects.get(username=client),
        session=Session.objects.get(pk=sess_num)
    )


@when('{trainer} creates a session of {activity} in {place} at {time}')
def step_impl(context, trainer, activity, place, time):
    from ActivityManager.models import Activity, Room, User
    from ActivityManager.views import create_session

    request = context.request_factory.get('/session/create')
    request.user = User.objects.get(username=trainer)
    request.POST = {
        'activity': Activity.objects.get(name__exact=activity).id,
        'room': Room.objects.get(name__exact=place).id,
        'timestamp': eval(time).strftime("%d/%m/%y - %H:%M"),
        'callback': '/'
    }
    create_session(request)


@when('{client} enrols to session {sess_num:d}')
def step_impl(context, client, sess_num):
    from ActivityManager.models import User
    from ActivityManager.views import enroll

    request = context.request_factory.get('/session/%d/enroll' % sess_num)
    request.user = User.objects.get(username=client)
    request.POST = {
        'callback': '/'
    }
    enroll(request, sess_num)


@then('{num:d} sessions exist')
def step_impl(context, num):
    from ActivityManager.models import Session
    assert Session.objects.count() == num


@then('{num:d} enrolments exist')
def step_impl(context, num):
    from ActivityManager.models import SessionUserControl
    assert SessionUserControl.objects.count() == num
