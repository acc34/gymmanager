Feature: Session creation
  Scenario: Create a session as trainer
    When trainer1 creates a session of activity1 in room1 at reference_time()
    Then 1 sessions exist

  Scenario: Create a session as client
    When client1 creates a session of activity1 in room1 at reference_time()
    Then 0 sessions exist

  Scenario: Create a session before now
    When trainer1 creates a session of activity1 in room1 at reference_time(before=True)
    Then 0 sessions exist