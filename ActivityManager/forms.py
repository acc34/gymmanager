from datetime import datetime, time

from django.forms import ModelForm, DateTimeField, forms

from ActivityManager.models import Session, SessionUserControl
from GymManager.settings import OPEN_HOURS


class SessionForm(ModelForm):
    timestamp = DateTimeField(disabled=True, initial=datetime.now())

    class Meta:
        model = Session
        exclude = ('trainer', 'timestamp')

    def is_valid(self):
        if not super().is_valid():
            return False

        timestamp = self.cleaned_data['timestamp']
        start_time, end_time = OPEN_HOURS

        if (
            timestamp.time().replace(hour=0) != time(0, 0, 0) or  # Check that time is a whole hour
            timestamp.weekday() > 4 or  # Check that day is workday
            timestamp.hour < start_time or timestamp.hour > end_time or  # Check that time is in the timetable
            timestamp < datetime.now()  # Check that time is after now
        ):
            return False

        same_time_sessions = Session.objects.filter(timestamp__exact=timestamp)
        if (
            len(same_time_sessions.filter(room__exact=self.cleaned_data['room']))  # No sessions in same room
        ):
            self.errors['room'] = ["Room is occupied at this time"]
            return False
        return True

    def clean(self):
        clean_data = super().clean()
        try:
            clean_datetime = datetime.strptime(self.data['timestamp'], '%d/%m/%y - %H:%M')
        except ValueError:
            raise forms.ValidationError("Wrong format for timestamp")
        clean_data['timestamp'] = clean_datetime
        return clean_data


class EnrolmentForm(ModelForm):
    class Meta:
        model = SessionUserControl
        exclude = ('user',)
