from django.contrib import admin
from ActivityManager import models

# Register your models here.
admin.site.register(models.Session)
admin.site.register(models.Activity)
admin.site.register(models.Room)
admin.site.register(models.SessionUserControl)
