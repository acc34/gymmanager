from datetime import *
from urllib.parse import parse_qs
import json

import django.http as http
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.template.defaulttags import register
from django.views.generic import ListView
from django.forms.models import model_to_dict

from ActivityManager.forms import SessionForm, EnrolmentForm
from ActivityManager.models import Session, SessionUserControl
from GymManager.settings import OPEN_HOURS


def format_hour(hour):
    return "%s:00" % str(hour).zfill(2)


def get_week_start(timestamp):
    timestamp = timestamp.date()
    return timestamp - timedelta(days=timestamp.weekday())


def assists_session(session, user):
    if not user.is_authenticated:
        return False
    return \
        len(SessionUserControl.objects.filter(session=session, user=user)) > 0 or \
        session.trainer == user


def filter_element(collection, element_id):
    candidates = collection.objects.filter(id=element_id)
    if not candidates.exists():
        return False, http.HttpResponseNotFound()

    candidates = candidates.filter(timestamp__gt=datetime.now())
    if not candidates.exists():
        return False, http.HttpResponseGone()
    return True, candidates.first()


def get_week(user, week_start):
    """
    Timetable object structure is:
    {
        <day (timestamp)>: {
            <time (string)>: [
                {
                    'session': <Session>,
                    'assisting': <boolean>
                }, ...
            ]
        }
    }
    """
    open_time, close_time = OPEN_HOURS
    week_end = week_start + timedelta(weeks=1)
    sessions = Session.objects.filter(timestamp__gte=week_start, timestamp__lt=week_end)
    timetable = {}

    for day in [week_start + timedelta(days=i) for i in range(5)]:
        for hour in range(open_time, close_time):
            timestamp = datetime.combine(day, time(hour=hour))
            timetable[timestamp] = list(map(
                lambda sess: {
                    'session': sess,
                    'assisting': assists_session(sess, user)
                },
                sessions.filter(timestamp__exact=timestamp)
            ))
    return timetable


@register.filter
def combine(d, t):
    return datetime.combine(d, t)


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter
def has_group(user, group_name):
    query = Group.objects.filter(name=group_name)
    if not query.exists():
        return False
    else:
        return query.first() in user.groups.all()


@register.filter
def disable_if_is_before(time1, time2):
    if time1 < time2:
        return "disabled"
    else:
        return ""


class TimetableView(ListView):
    model = Session
    template_name = 'ActivityManager/timetable.html'

    def get_context_data(self, **kwargs):
        current_week = get_week_start(datetime.now())
        selected_week = current_week
        open_time, close_time = OPEN_HOURS

        query_string = parse_qs(self.request.GET.urlencode())
        if 'week' in query_string and len(query_string['week']) == 1:
            try:
                timestamp = int(query_string['week'][0].strip('/'))
                selected_week = get_week_start(datetime.fromtimestamp(timestamp))
                if selected_week < current_week:
                    selected_week = current_week
            except ValueError:
                pass

        context = super().get_context_data(**kwargs)
        context['timetable'] = get_week(self.request.user, selected_week)
        context['days'] = list(map(lambda i: selected_week + timedelta(days=i), range(5)))
        context['times'] = [time(hour=hour) for hour in list(range(open_time, close_time))]

        context['form'] = SessionForm()

        context['week'] = {
            'now': int(datetime.timestamp(datetime.combine(current_week, time()))),
            'prev': int(datetime.timestamp(datetime.combine(selected_week - timedelta(days=7), time()))),
            'curr': int(datetime.timestamp(datetime.combine(selected_week, time()))),
            'next': int(datetime.timestamp(datetime.combine(selected_week + timedelta(days=7), time())))
        }
        context['now'] = datetime.now()
        return context


@login_required(redirect_field_name='/activities')
def create_session(request):
    if not has_group(request.user, 'trainer'):
        return http.HttpResponseForbidden()
    form = SessionForm(request.POST)
    if form.is_valid():
        form_data = form.cleaned_data
        if len(Session.objects.filter(trainer__exact=request.user, timestamp__exact=form_data['timestamp'])):
            return http.HttpResponseBadRequest("A session already exists for you at this time")
        Session(
            trainer=request.user,
            timestamp=form_data['timestamp'],
            room=form_data['room'],
            activity=form_data['activity']
        ).save()
        return http.HttpResponse()

    errors = []
    for error in form.errors.keys():
        for message in form.errors[error]:
            errors.append("%s: %s" % (error, message))
    return http.HttpResponseBadRequest(json.dumps(errors))


@login_required(redirect_field_name='/activities')
def delete_session(request, session_id):
    # We check here so that user does not get info about the error if they are not trainer
    if not has_group(request.user, 'trainer'):
        return http.HttpResponseForbidden()

    found, session = filter_element(Session, session_id)
    if not found:
        return session

    if session.trainer != request.user:
        return http.HttpResponseForbidden()
    session.delete()
    return http.HttpResponse()


@login_required(redirect_field_name='/activities')
def enroll(request, session_id):
    if not has_group(request.user, 'client'):
        return http.HttpResponseForbidden()

    found, session = filter_element(Session, session_id)
    if not found:
        return session

    # Check if user already has an activity at this time
    if len(SessionUserControl.objects.filter(user=request.user, session__timestamp__exact=session.timestamp)):
        return http.HttpResponseBadRequest("An enrolment at this time already exists")

    # Check if session is full
    num_assistants = len(SessionUserControl.objects.filter(session=session))
    max_assistants = min(session.room.max_assistants, session.activity.max_assistants)
    if num_assistants == max_assistants:
        return http.HttpResponseBadRequest("Session is full")

    SessionUserControl(
        user=request.user,
        session=session
    ).save()
    return http.HttpResponse()


@login_required(redirect_field_name='/activities')
def unenroll(request, session_id):
    # No user filtering needed because only clients can enroll anyway
    found, session = filter_element(Session, session_id)
    if not found:
        return session

    SessionUserControl.objects.filter(user=request.user, session=session).delete()
    return http.HttpResponse()


def get_session_details(request, session_id):
    sess_list = Session.objects.filter(id=session_id)
    if len(sess_list) == 0:
        return http.HttpResponseNotFound()

    sess = sess_list.first()
    return http.HttpResponse(json.dumps({
        'activity': sess.activity.name,
        'room': sess.room.name,
        'trainer': sess.trainer.username,
        'max_assistants': min(sess.activity.max_assistants, sess.room.max_assistants),
        'assistants': list(map(
            lambda enrolment: enrolment.user.username,
            SessionUserControl.objects.filter(session=sess)
        ))
    }))
