from django.contrib.auth.models import User
from django.db import models


class Activity(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    max_assistants = models.SmallIntegerField()

    def __str__(self):
        return self.name


class Room(models.Model):
    name = models.CharField(max_length=50)
    max_assistants = models.SmallIntegerField()

    def __str__(self):
        return self.name


class Session(models.Model):
    activity = models.ForeignKey(Activity, null=False, blank=False, on_delete=models.PROTECT)
    room = models.ForeignKey(Room, null=False, blank=False, on_delete=models.PROTECT)
    timestamp = models.DateTimeField(null=False, blank=False)
    trainer = models.ForeignKey(User, null=False, blank=False, on_delete=models.SET("[deleted]"))

    def __str__(self):
        return self.activity.name + " in " + self.room.name + " at " + self.timestamp.isoformat()


class SessionUserControl(models.Model):
    session = models.ForeignKey(Session, null=False, blank=False, on_delete=models.PROTECT)
    user = models.ForeignKey(User, null=False, blank=False, on_delete=models.PROTECT)

    def __str__(self):
        return str(self.id) + " - " + str(self.session) + " - " + self.user.username
