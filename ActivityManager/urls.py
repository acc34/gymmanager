"""ActivityManager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')pip
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf.urls import url
from ActivityManager.views import TimetableView, create_session, unenroll, enroll, get_session_details, delete_session

app_name = "ActivityManager"

urlpatterns = [
    url(r'^$', TimetableView.as_view(), name="timetable_view"),
    url(r'^session/create/$', create_session, name="session_create"),
    url(r'^session/(?P<session_id>[0-9]*)/unenroll/$', unenroll, name="unenroll"),
    url(r'^session/(?P<session_id>[0-9]*)/enroll/$', enroll, name="enroll"),
    url(r'^session/(?P<session_id>[0-9]*)/delete/$', delete_session, name="session_delete"),
    url(r'^session/(?P<session_id>[0-9]*)/$', get_session_details, name="session_details"),
]
