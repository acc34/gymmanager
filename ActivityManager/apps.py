from django.apps import AppConfig


class ActivitymanagerConfig(AppConfig):
    name = 'ActivityManager'
